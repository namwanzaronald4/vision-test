# vision_test

A Vision developer test.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## FontEnd :: TO-DO
Implement any of the screens in the SCREENS directory from the project

NOTE: Use mockup data

1. Home screen

2. Notification screen

3. Notification settings screen

4. Login screen

-> Create a Pull Request for the work you have done with clear description

## BackEnd :: TO-DO

For the above screens, create a RESTful API for any of the two screens, in C#

NOTE:

- You can use mockup data
- You are required to create a GitLab repo for the api created

